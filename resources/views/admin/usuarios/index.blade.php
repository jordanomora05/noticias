@extends('layouts.padre')
@section('contenido')

<br>
<h1 align="center"> Gestion de usuario </h1>

<div class="row">
	<div class="col-ig-12">
		<table class="table" width="80%">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nombre de usuario</th>
					<tbody>
						@foreach ($usuarios as $item)
						<tr>
							<td>{{ $item->id }}</td>
							<td>{{ $item->username }}</td>
							<td> <a class="btn btn-succes" href="usuario/{{ $item->id }}">Editar</a> </td>
						</tr>
						@endforeach
					</tbody>
				</tr>
			</thead>
		</table>
	</div>
</div>

@stop